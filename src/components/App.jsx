
import WeatherWidget from "./WeatherWidget";

const App = () => {
  
  return (
    <main className="weather-container">
      <WeatherWidget />
    </main>
  );
};

export default App;
