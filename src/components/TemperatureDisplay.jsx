import PropTypes from 'prop-types'

const TemperatureDisplay = (props) => {
  const { min, max, avg } = props // Ceci aide à savoir au préalable quelles sont les props du composant
  
  return (
    <article className="today">
      <div className="temperature-display">
        <p className="temperature-display-avg">{avg}</p>
        <div className="temperature-display-row">
          <p className="temperature-display-row-item--min">{min}</p>
          {" "}
          <p className="temperature-display-row-item--max">{max}</p>
        </div>
      </div>
    </article>
  );
};

TemperatureDisplay.propTypes = {
  max: PropTypes.number.isRequired,
  min: PropTypes.number.isRequired,
  avg: PropTypes.number.isRequired,
}

export default TemperatureDisplay;
