import React, { useState, useEffect } from "react";
import TemperatureDisplay from "./TemperatureDisplay";
import WeatherCode from "./WeatherCode";
import ForecastItem from "./ForecastItem";

// Constantes du projet
const baseUrl = "https://api.open-meteo.com/v1/forecast";
const timezone = "Europe/London";
const daily = ["weathercode", "temperature_2m_max", "temperature_2m_min"];

const hourly = ["temperature_2m", "weathercode"];

// Les coordonnées de La Rochelle
const latitude = 46.1592;
const longitude = -1.171;

// Composant
const WeatherWidget = () => {
  const [appState, setAppState] = useState(null);
  const [currentTab, setCurrentTab] = useState("hourly");

  const fetchData = () => {
    const apirUrl = `${baseUrl}?latitude=${latitude}&longitude=${longitude}&daily=${daily.join(
      ","
    )}&hourly=${hourly.join(",")}&timezone=${timezone}`;
    fetch(apirUrl)
      .then((res) => res.json())
      .then((data) =>
        setAppState({
          meteo: {
            ...data,
            current: {
              time: data.daily?.time[0],
              weathercode: data.daily?.weathercode[0],
              temperature_min: data.daily?.temperature_2m_min[0],
              temperature_max: data.daily?.temperature_2m_max[0],
              temperature_avg:
                (data.daily?.temperature_2m_max[0] +
                  data.daily?.temperature_2m_min[0]) /
                2,
            },
          },
          timestamp: new Date().getTime(),
        })
      )
      .catch((error) =>
        console.error("Erreur lors de la récupération des données:", error)
      );
  };

  useEffect(() => {
    // Applique une fois au départ
    fetchData();
    const interval = setInterval(fetchData, 10000);
    return () => {
      clearInterval(interval);
    };
  }, []); // Et ne se réapplique pas car on a pas spécifié de variables à suivre dans le tableau

  return (
    <div className="weather-container-content">
      <header className="weather-container-header">
        <p className="location">La Rochelle</p>
        <button className="refresh-button" onClick={fetchData}>
          <img
            src="https://lpmiaw-react.napkid.dev/img/weather/refresh.png"
            alt="Refresh"
          />
        </button>
      </header>
      {/* On affiche suivant les données qu'on a récupérer */}
      {appState?.meteo?.current && (
        <p className="date">{appState?.meteo?.current?.time}</p>
      )}
      {appState?.meteo?.current && (
        <TemperatureDisplay
          min={appState?.meteo?.current?.temperature_min}
          max={appState?.meteo?.current?.temperature_max}
          avg={appState?.meteo?.current?.temperature_min}
        />
      )}
      {appState?.meteo?.current && (
        <WeatherCode code={appState?.meteo?.current?.weathercode} />
      )}
      <section className="">
        <nav className="tabs">
          <button
            onClick={() => setCurrentTab("hourly")}
            // Si on est en hourly alors tab--active
            className={currentTab === "hourly" ? "tab tab--active" : "tab"}
          >
            Journée
          </button>
          <button
            onClick={() => setCurrentTab("daily")}
            // Si on est en daily alors tab--active
            className={currentTab === "daily" ? "tab tab--active" : "tab"}
          >
            Semaine
          </button>
        </nav>
        <ul className="forecast">
          {" "}
          {currentTab === "hourly"
            ? appState &&
              appState.meteo &&
              appState.meteo.hourly.time
                .slice(0, 5) // On récupère les 5 première valeurs
                .map(
                  (
                    timeValue,
                    idx // On utilise nos données pour le ForecastItem
                  ) => (
                    <ForecastItem
                      key={idx}
                      label={new Date(
                        appState?.meteo?.hourly?.time[idx]
                      ).getUTCHours()}
                      code={appState?.meteo?.hourly?.weathercode[idx]}
                      temperature={appState?.meteo?.hourly?.temperature_2m[idx]}
                    />
                  )
                )
            : appState &&
              appState.meteo &&
              appState.meteo.daily.time
                .slice(0, 5) // On récupère les 5 première valeurs
                .map(
                  (
                    timeValue,
                    idx // On utilise nos données pour le ForecastItem
                  ) => (
                    <ForecastItem
                      key={idx}
                      label={
                        new Date(
                          appState?.meteo?.daily?.time[idx]
                        ).getUTCDate() +
                        "/" +
                        (new Date(
                          appState?.meteo?.daily?.time[idx]
                        ).getUTCMonth() +
                          1)
                      }
                      code={appState?.meteo?.daily?.weathercode[idx]}
                      temperature={
                        (appState?.meteo?.daily?.temperature_2m_max[idx] +
                          appState?.meteo?.daily?.temperature_2m_min[idx]) /
                        2
                      }
                    />
                  )
                )}{" "}
        </ul>
      </section>
      <footer className="weather-container-footer">
        {appState && (
          <p>
            Mis à jours à {new Date(appState.timestamp).toLocaleTimeString()}
          </p>
        )}
      </footer>
    </div>
  );
};

export default WeatherWidget;
